<?php

namespace Drupal\consent\Storage;

/**
 * Class ConsentStorageException.
 */
class ConsentStorageException extends \RuntimeException {}
