<?php

/**
 * @file
 * Preprocess html for mytheme theme.
 */

use Drupal\Core\Url;

/**
 * Implements template_preprocess_html().
 */
function mytheme_preprocess_html(&$variables) {
  // For being able to test cross-origin content:
  //   1. use a different sub-domain.
  //      Example: When having https://mysite.local/some-url?amp, use https://www.mysite.local/some-url?amp instead.
  //   2. You need to setup a local SSL certificate too.
  //      I found a good example to setup a local cert here:
  //      https://medium.freecodecamp.org/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec
  $consent_iframe_url = str_replace('www.', '',
    Url::fromRoute('consent_iframe')->setAbsolute(TRUE)->toString());
  if (!(strpos($consent_iframe_url, 'https:') === 0)) {
    // iFrame src must begin with https.
    $consent_iframe_url = 'https:' . str_replace('http:', '', $consent_iframe_url);
  }
  $variables['consent_iframe_url'] = $consent_iframe_url;
}
